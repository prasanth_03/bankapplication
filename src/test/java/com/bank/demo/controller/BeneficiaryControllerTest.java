package com.bank.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bank.controller.BeneficiaryController;
import com.bank.dto.BeneficiaryDTO;
import com.bank.dto.BeneficiaryResponseDTO;
import com.bank.service.BeneficiaryService;

@ExtendWith(MockitoExtension.class)
class BeneficiaryControllerTest {

	@Mock
	private BeneficiaryService beneficiaryService;

	@InjectMocks
	private BeneficiaryController beneficiaryController;

	@Test
	void testAddBeneficiary_Success() {

		BeneficiaryDTO beneficiaryDTO = new BeneficiaryDTO();
		beneficiaryDTO.setBeneficiaryName("John Doe");
		beneficiaryDTO.setBeneficiaryAccountNo("1234567890");
		beneficiaryDTO.setBeneficiaryBankName("Bank of XYZ");
		beneficiaryDTO.setBeneficiaryBranch("Branch ABC");
		beneficiaryDTO.setBeneficiaryIFSCCode("XYZ1234567");

		String customerId = "123";
		BeneficiaryResponseDTO responseDTO = new BeneficiaryResponseDTO("Beneficiary Added Successfully...",
				customerId);

		when(beneficiaryService.addBeneficiary(customerId, beneficiaryDTO)).thenReturn(responseDTO);

		ResponseEntity<BeneficiaryResponseDTO> responseEntity = beneficiaryController.addBeneficiary(customerId,
				beneficiaryDTO);

		assertNotNull(responseEntity);
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals("Beneficiary Added Successfully...", responseEntity.getBody().getMessage());
		assertEquals(customerId, responseEntity.getBody().getCustomerId());
	}
}