package com.bank.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bank.dto.AccountDTO;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.ResourceNotFound;
import com.bank.repository.AccountRepository;
import com.bank.service.AccountService;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {
	@Mock
	private AccountRepository accountRepository;
	@InjectMocks
	private AccountService accountService;

	@Test
	void testGetDataByColumnName_WithValidCustomerId() {

		String customerId = "123";
		CustomerRegistration registration = new CustomerRegistration();
		registration.setCustomerId(customerId);
		when(accountRepository.findByCustomerId(customerId)).thenReturn(List.of(registration));

		List<AccountDTO> result = accountService.getDataByColumnName(customerId);

		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(customerId, result.get(0).getCustomerId());
	}

	@Test
	void testGetDataByColumnName_WithInvalidCustomerId() {

		String invalidCustomerId = "";

		ResourceNotFound exception = assertThrows(ResourceNotFound.class, () -> {
			accountService.getDataByColumnName(invalidCustomerId);
		});
		assertEquals("customerId must not be blank", exception.getMessage());

		verify(accountRepository, never()).findByCustomerId(anyString());
	}
}