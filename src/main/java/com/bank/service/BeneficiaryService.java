package com.bank.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bank.dto.BeneficiaryDTO;
import com.bank.dto.BeneficiaryResponseDTO;
import com.bank.dto.LogInStatus;
import com.bank.entity.Beneficiary;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.ResourceNotFound;
import com.bank.repository.BeneficiaryRepository;
import com.bank.repository.CustomerRegistrationRepository;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service

public class BeneficiaryService {

	private BeneficiaryRepository beneficiaryRepository;

	private CustomerRegistrationRepository registrationRepository;

	public BeneficiaryResponseDTO addBeneficiary(String customerId, BeneficiaryDTO beneficiaryDTO) {
		Optional<CustomerRegistration> registrationOptional = registrationRepository.findByCustomerIdAndLogged(customerId, LogInStatus.LOGIN);
		if (registrationOptional.isPresent()) {
			CustomerRegistration registration = registrationOptional.get();

			if (beneficiaryRepository.existsByBeneficiaryAccountNo(beneficiaryDTO.getBeneficiaryAccountNo())) {
				throw new ResourceNotFound("Beneficiary with the same account number already exists");
			}

			Beneficiary beneficiary = new Beneficiary();
			beneficiary.setBeneficiaryName(beneficiaryDTO.getBeneficiaryName());
			beneficiary.setBeneficiaryAccountNo(beneficiaryDTO.getBeneficiaryAccountNo());
			beneficiary.setBeneficiaryBankName(beneficiaryDTO.getBeneficiaryBankName());
			beneficiary.setBeneficiaryBranch(beneficiaryDTO.getBeneficiaryBranch());
			beneficiary.setBeneficiaryIFSCCode(beneficiaryDTO.getBeneficiaryIFSCCode());
			beneficiary.setRegistration(registration);
			beneficiaryRepository.save(beneficiary);

			String message = "Beneficiary Added Successfully...";
			BeneficiaryResponseDTO responsedto = new BeneficiaryResponseDTO(message, registration.getCustomerId());
			return responsedto;
		} else {
			throw new ResourceNotFound("Please Login to add beneficiary details");
		}
	}
}