package com.bank.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.bank.dto.AccountDTO;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.ResourceNotFound;
import com.bank.repository.AccountRepository;

import jakarta.validation.constraints.NotBlank;

@Service
public class AccountService {
	private final AccountRepository accountRepository;

	public AccountService(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	public List<AccountDTO> getDataByColumnName(@NotBlank String customerId) {

		if (customerId == null || customerId.trim().isEmpty()) {
			throw new ResourceNotFound("customerId must not be blank");
		}
		List<CustomerRegistration> entities = accountRepository.findByCustomerId(customerId);

		if (entities.isEmpty()) {
			throw new ResourceNotFound("Customer with ID " + customerId + " not found");
		}
		return entities.stream().map(this::mapEntityToDTO).collect(Collectors.toList());
	}

	private AccountDTO mapEntityToDTO(CustomerRegistration entity) {
		AccountDTO dto = new AccountDTO();
		dto.setCustomerId(entity.getCustomerId());
		dto.setAccountNo(entity.getAccountNo());
		dto.setIfscCode(entity.getIfscCode());
		dto.setBankName(entity.getBankName());
		dto.setBranch(entity.getBranch());
		dto.setBalance(entity.getBalance());
		return dto;
	}
}
