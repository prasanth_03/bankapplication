package com.bank.exception;

public class IllegalException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IllegalException(String message) {
		super(message);
	}
}
