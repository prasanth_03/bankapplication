package com.bank.exception;

import java.net.http.HttpHeaders;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bank.dto.loginresponse;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
				HttpHeaders headers, HttpStatus status, WebRequest request) {
			Map<String, String> errors = new HashMap<>();
			ex.getBindingResult().getAllErrors().forEach(error -> {
				String fieldName = ((FieldError) error).getField();
				String message = error.getDefaultMessage();
				errors.put(fieldName, message);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
		}

	@ExceptionHandler(ResourceNotFound.class)
	public ResponseEntity<loginresponse> handleResourceNotFound(ResourceNotFound exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new loginresponse(exception.getMessage()));

	}
	
	@ExceptionHandler(ProfileNotFoundException.class)
	public ResponseEntity<loginresponse> handleProfileNotFoundException(ProfileNotFoundException exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new loginresponse(exception.getMessage()));

	}
	
	@ExceptionHandler(UserAlreadyExists.class)
	public ResponseEntity<loginresponse> handleUserAlreadyExists(UserAlreadyExists exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new loginresponse(exception.getMessage()));

	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<loginresponse> handleResourceNotFoundException(ResourceNotFoundException ex) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new loginresponse(ex.getMessage()));
	}

	@ExceptionHandler(InsufficientBalanceException.class)
	public ResponseEntity<loginresponse> handleInsufficientBalanceException(InsufficientBalanceException ex) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new loginresponse(ex.getMessage()));
	}
}
