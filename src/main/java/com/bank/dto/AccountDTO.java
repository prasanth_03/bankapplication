package com.bank.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class AccountDTO {

	@NotBlank(message = "Customer ID is required")
	private String customerId;
	@NotBlank(message = "Account number is required")
	private String accountNo;
	@NotBlank(message = "IFSC code is required")
	private String ifscCode;
	@NotBlank(message = "Bank name is required")
	private String bankName;
	@NotBlank(message = "Branch is required")
	private String branch;
	private double balance;
}
