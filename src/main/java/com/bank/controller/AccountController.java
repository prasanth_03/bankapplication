package com.bank.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dto.AccountDTO;
import com.bank.service.AccountService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class AccountController {

	private AccountService service;

	@GetMapping("/account")
	public List<AccountDTO> getData(@RequestParam("Customer ID") String customerId) {
		return service.getDataByColumnName(customerId);
	}
}
